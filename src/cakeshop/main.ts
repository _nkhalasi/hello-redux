import { createStore } from "redux"
import { reducer } from "./reducers"
import { buyCake, buyIcecream } from "./actions"

const store = createStore(reducer)
console.log("Initial state ", store.getState())
const unsubscribe = store.subscribe(() => { console.log("Updated state ", store.getState()) })
store.dispatch(buyCake())
store.dispatch(buyCake())
store.dispatch(buyCake())
store.dispatch(buyIcecream())
store.dispatch(buyIcecream())
store.dispatch(buyIcecream())
store.dispatch(buyIcecream())
unsubscribe()
