import { CakeShopState, CustomerActions, BUY_CAKE, BUY_ICECREAM } from "./types"

const initialState: CakeShopState = {
  cake: { numOfCakes: 10 },
  icecream: { numOfIcecreams: 15 }
}

export const reducer = (prevState: CakeShopState = initialState, action: CustomerActions) => {
  switch (action.type) {
    case BUY_CAKE: return {
      ...prevState,
      cake: { numOfCakes: prevState.cake.numOfCakes - 1 }
    }
    case BUY_ICECREAM: return {
      ...prevState,
      icecream: { numOfIcecreams: prevState.icecream.numOfIcecreams - 1 }
    }
    default: return prevState
  }
}
