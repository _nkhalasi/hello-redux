export interface CakeStockState {
  numOfCakes: number
}
export interface IcecreamStockState {
  numOfIcecreams: number
}
export interface CakeShopState {
  cake: CakeStockState,
  icecream: IcecreamStockState
}

export const BUY_CAKE: string = "BUY_CAKE"
export const BUY_ICECREAM: string = "BUY_ICECREAM"

export interface Action {
  type: string
}
export interface BuyCakeAction extends Action {
  info: string
}
export interface BuyIcecreamAction extends Action {
  when: Date
}

export type CustomerActions = BuyCakeAction | BuyIcecreamAction
