import { createStore } from "redux"
import { BuyCakeAction, BuyIcecreamAction, BUY_CAKE, BUY_ICECREAM, CakeShopState, CustomerActions } from "./types"

export function buyCake(): BuyCakeAction {
  return {
    type: BUY_CAKE,
    info: "Cake shop buy action"
  }
}

export function buyIcecream(): BuyIcecreamAction {
  return {
    type: BUY_ICECREAM,
    when: new Date()
  }
}
